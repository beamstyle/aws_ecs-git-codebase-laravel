#!/bin/bash
set -e

# Git - Variables
# NOTE: GIT_REPOSITORY_URL should include basic credentials (eg. https://username:password-or-api-key@bitbucket.org/organization/project.git)
WWW_ROOT=${WWW_ROOT}
GIT_REPOSITORY_URL=${GIT_REPOSITORY_URL}
GIT_REPOSITORY_BRANCH=${GIT_REPOSITORY_BRANCH}

# Git - Retrieving source
cd $WWW_ROOT
rm -rf $WWW_ROOT/*
export GIT_TRACE_PACKET=1
export GIT_TRACE=1
export GIT_CURL_VERBOSE=1
git config --global http.postBuffer 157286400
git config --global --add core.compression -1
git clone -b $GIT_REPOSITORY_BRANCH $GIT_REPOSITORY_URL $WWW_ROOT

# Laravel - Variables
LARAVEL_DIR=${LARAVEL_DIR}

# Laravel - Composer - Credentials
mkdir -p /root/.composer/
COMPOSER_BITBUCKET_USERNAME=${COMPOSER_BITBUCKET_USERNAME}
COMPOSER_BITBUCKET_PASSWORD=${COMPOSER_BITBUCKET_PASSWORD}
COMPOSER_GITHUB_ACCESS_TOKEN=${COMPOSER_GITHUB_ACCESS_TOKEN}
export COMPOSER_BITBUCKET_USERNAME=$COMPOSER_BITBUCKET_USERNAME
export COMPOSER_BITBUCKET_PASSWORD=$COMPOSER_BITBUCKET_PASSWORD
export COMPOSER_GITHUB_ACCESS_TOKEN=$COMPOSER_GITHUB_ACCESS_TOKEN
envsubst < /tmp/auth.json > /root/.composer/auth.json

# Laravel - Composer - composer install
cd "$WWW_ROOT""$LARAVEL_DIR"
composer install

# Laravel - Directory Permissions
chgrp -R www-data "$WWW_ROOT""$LARAVEL_DIR"/bootstrap/cache
chmod -R ug+rwx "$WWW_ROOT""$LARAVEL_DIR"/bootstrap/cache
chgrp -R www-data "$WWW_ROOT""$LARAVEL_DIR"/storage
chmod -R ug+rwx "$WWW_ROOT""$LARAVEL_DIR"/storage

# Laravel - Cron Variables (Optional)
CRON_SCRIPT_PATH=${CRON_SCRIPT_PATH}
CRON_SCHEDULE=${CRON_SCHEDULE}

if [[ ! -z "${CRON_SCRIPT_PATH}" ]]; then
    # Schedule the cron job
    # CRON_LOG="$WWW_ROOT$LARAVEL_DIR/storage/logs/cron-\$(date '+%Y-%m-%d').log" # This does not work because we use "tail -f" to keep the task in foreground.
    CRON_LOG="$WWW_ROOT$LARAVEL_DIR/storage/logs/cron.log"
    echo "$CRON_SCHEDULE www-data bash ""$CRON_SCRIPT_PATH"" 2>&1 | tee -a ""$CRON_LOG""" | tee --append /etc/crontab

    # Create CRON_LOG file, and adjust permissions
    echo "" >> "$CRON_LOG"
    chgrp -R www-data "$CRON_LOG"
    chmod -R ug+rwx "$CRON_LOG"

    # Run the cron, and watch the CRON_LOG file
    cron
    tail -f "$CRON_LOG"
fi
